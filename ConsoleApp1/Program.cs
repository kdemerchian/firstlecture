﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            String();

            Console.ReadKey();
        }

        static void ValueTypeExample()
        {
            State state1 = new State(); // Структура State
            State state2 = new State();
            state2.x = 1;
            state2.y = 2;
            state1 = state2;
            state2.x = 5; // state1.x=1 по-прежнему
            Console.WriteLine(state1.x); // 1
            Console.WriteLine(state2.x); // 5
        }

        static void ReferenceTypeExamples()
        {
            Country country1 = new Country(); // Класс Country
            Country country2 = new Country();
            country2.x = 1;
            country2.y = 4;
            country1 = country2;
            country2.x = 7; // теперь и country1.x = 7, так как обе ссылки и country1 и country2 
            // указывают на один объект в куче
            Console.WriteLine(country1.x); // 7
            Console.WriteLine(country2.x); // 7
        }

        static void DataTypes()
        {
            int intMinVal = int.MinValue;
            int intMaxVal = int.MaxValue;

            short t = short.MinValue;
            t -= 1;
        }

        static void LogicalOperator()
        {
            bool a = false;
            bool b = true;

            Console.WriteLine(a && b); // false
            Console.WriteLine(a || b); // true
        }

        static void ConditionalOperator()
        {
            int randValue = new Random().Next(1, 100);
            string str = randValue > 50 ? "More then 50" : "Less or equal 50";
            Console.WriteLine($"Random value: {randValue}, text: '{str}'");
        }

        static void InstanceOf()
        {
            object objCountry = new Country(); //class
            object objState = new State(); // struct

            Country country = new Country(); //class
            State state = new State(); //struct

            TestFolder.Country country2 = new TestFolder.Country();

            Console.WriteLine($"TypeOf 'Country' object: {typeof(Country)}");
            Console.WriteLine($"TypeOf 'State' object: {typeof(State)}");

            Console.WriteLine();

            Console.WriteLine($"{objCountry.GetType() == typeof(Country)}"); //true
            Console.WriteLine($"{objState.GetType() == typeof(Country)}"); //false
            Console.WriteLine($"{country2.GetType() == typeof(Country)}"); //false

            Console.WriteLine();

            Console.WriteLine($"NameOf 'Country' object: {nameof(country)}");
            Console.WriteLine($"NameOf 'State' object: {nameof(state)}");

            Console.WriteLine();

            object[] values = { 225, "12345" };
            foreach (var value in values)
            {
                Type tp = value.GetType();

                if (tp.Equals(typeof(int)))
                    Console.WriteLine($"{value} is an integer data type.");

                else
                    Console.WriteLine($"'{value}' is not an int data type.");
            }
        }

        static void IfElse()
        {
            int testScore = 75;
            string str1 = string.Empty;

            if (testScore >= 90)
            {
                str1 += "A";
                testScore += 1;
            }
            else if (testScore >= 80)
                str1 += "B";
            else if (testScore >= 70)
                str1 += "C";
            else if (testScore >= 60)
                str1 += 'D';
            else
                str1 += "F";


            Console.WriteLine($"String 1: {str1}");

            string str2 = string.Empty;
            testScore = 88;

            if (testScore >= 90)
                str2 += "A";
            if (testScore >= 80)
                str2 += "B";
            if (testScore >= 70)
                str2 += "C";
            if (testScore >= 60)
                str2 += 'D';
            else
                str2 += "F";

            Console.WriteLine($"String 2: {str2}");

            Console.ReadKey();
        }

        static void SwitchCase()
        {
            Random rand = new Random();
            int caseSwitch = rand.Next(1, 10);

            switch (caseSwitch)
            {
                case 1:
                    Console.WriteLine("Case 1");
                    break;
                case 2:
                case 3:
                    Console.WriteLine($"Case {caseSwitch}");
                    break;
                case 4:
                    goto case 3;
                case 5:
                    goto default;
                default:
                    Console.WriteLine($"An unexpected value ({caseSwitch})");
                    break;
            }
        }

        static void LoopingFor()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Number of iteration: {i}");
            }

            //for (int i = 0; i < 10; i++)
            //    Console.WriteLine($"Number of iteration: {i}");
        }

        static void LoopingForEach()
        {
            string str = "test";

            foreach (char c in str)
                Console.WriteLine(c);
        }

        static void While()
        {
            int val = 10;

            while (val != 0)
            {
                Console.WriteLine(val);
                val--;
            }
        }

        static void DoWhile()
        {
            int val = 10;

            do
            {
                Console.WriteLine(val);
                val--;
            } while (val != 0);
        }

        static void Break()
        {
            Console.WriteLine("For looping");
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    Console.WriteLine("Here is continue");
                    continue;
                }

                if (i == 7)
                {
                    Console.WriteLine("Here is break");
                    break;
                }

                Console.WriteLine($"Number of iteration: {i}");
            }

            Console.WriteLine();
            Console.WriteLine("Foreach looping");
            string str = "test string";

            foreach (char c in str)
            {
                if (c == 's')
                {
                    Console.WriteLine("Here is continue");
                    continue;
                }

                if (c == 'r')
                {
                    Console.WriteLine("Here is break");
                    break;
                }

                Console.WriteLine(c);
            }

            Console.WriteLine();
            Console.WriteLine($"While looping");
            int val = 10;
            while (val != 0)
            {
                if (val == 7)
                {
                    Console.WriteLine("Here is continue");
                    val--;
                    continue;
                }

                if (val == 5)
                {
                    Console.WriteLine("Here is break");
                    break;
                }

                Console.WriteLine(val);
                val--;
            }

            Console.WriteLine();
            Console.WriteLine($"Do While looping");
            int val2 = 10;
            do
            {
                if (val2 == 7)
                {
                    Console.WriteLine("Here is continue");
                    val2--;
                    continue;
                }

                if (val2 == 5)
                {
                    Console.WriteLine("Here is break");
                    break;
                }

                Console.WriteLine(val2);
                val2--;
            } while (val2 != 0);
        }

        static void Array()
        {
            string str = "Some test string";
            char[] charArr = str.ToCharArray();
            
            int[] intArr = new int[5];
            //int[] intArr = new int[] { 0, 0, 0, 0, 0 }; // the same
            int[] intArr3 = new int[] { 100, 2000, 30, 40, 50 };


            int[,] intArray1 = new int[3, 2];
            //int[,] intArray1 = new int[ , ]{ {0, 0}, {0, 0}, {0, 0} }; // the same

            int[,] intArray2 = { { 1, 1 }, { 1, 2 }, { 1, 3 } };

            object[] objArray = new object[] { 'A', 5, "test", 4.5m }; // array with
            //different types of object
        }

        static void String()
        {
            int value1 = 1;
            int value2 = 2;

            //simple string
            string simpleStr = "Value1: 1, value2: 2"; // hardcode without external parameters
            Console.WriteLine(simpleStr);

            //concatinations
            string concatString = "Value1: " + value1 + ", value2: " + value2;
            Console.WriteLine(concatString);

            // String.Format
            string formatStr = string.Format("Value1: {0}, value2: {1}", value1, value2);
            Console.WriteLine(formatStr);

            //interpolations
            string interpol = $"Value1: {value1}, value2: {value2}";
            Console.WriteLine(interpol);
        }

    }
}
