﻿namespace ConsoleApp1
{
    public class Country
    {
        public int x { get; set; }
        public int y { get; set; }
    }

    public struct State
    {
        public int x;
        public int y;
        public Country country;
    }
}
